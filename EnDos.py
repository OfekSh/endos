#!/bin/python3
import os
from bcc import BPF
import ctypes
import time
import threading
import subprocess

cCode = '''
#include <linux/in.h> //for struct in_addr
#include <linux/types.h>
#include <asm/ptrace.h>
#include <asm/socket.h>
#include <linux/skbuff.h>
#include <linux/ip.h>

BPF_PERF_OUTPUT(events);

struct NetworkStats
{
	__u32 sIp;
	__u32 dIp;
};

int icmp_trace(struct pt_regs * ctx)
{
	struct sk_buff skb = {};
	struct AddressData *data = 0;

	bpf_probe_read(&skb, sizeof(skb), (void*)PT_REGS_PARM1(ctx)); 		//Receiving skb.

	struct iphdr ip_header = {};
	bpf_probe_read(&ip_header, sizeof(ip_header), (struct iphdr *)skb_network_header(&skb));	//Receiving Src and Dst Ip

	struct NetworkStats currStats = {};			//Setting the Ip addresses to the
	currStats.sIp = ip_header.saddr;			//struct that is sended to the python.
	currStats.dIp = ip_header.daddr;
	events.perf_submit(ctx, &currStats, sizeof(currStats)); //Submitting the struct to the python.
	return 0;
}

'''
TIME_PER_INTERVAL = 1.0 #10 seconds is the time per interval.
MAX_PINGS_PER_TIME = 5
address_list = {} #Creating a dict, will be in form: { IP : [ currentTime, counter] }

lock = threading.Lock() #Creating the Lock object. Used to prevent double ip bans.
banned_ips = []


#class InAddr(ctypes.Structure):
#    _fields_ = [ ('s_addr', ctypes.c_uint) ]

class MyStats(ctypes.Structure):
	_fields_ = [("sIp" , ctypes.c_uint),
				("dIp" , ctypes.c_uint)]

def is_unwanted_address(address):
	'''
	Don't want to block localhost.
	'''
	return address == "127.0.0.1"

def remove_unwanted_addresses():
	'''
	Removing the unwanted addresses -> addresses that stopped pinging the server and just take memory.
	'''
	list_to_rem = []
	for item in address_list.keys():
		if time.time() - address_list[item][0] > TIME_PER_INTERVAL:
			list_to_rem.append(item)
	for item in list_to_rem:
		del address_list[item]

def check_and_block(source_ip):
	'''
	This function checks if the ip that pinged the server is considered as "spamming" the server.
				Input:
	source_ip: The ip to check.
	'''
	global address_list, banned_ips
	remove_unwanted_addresses() #First of all, removing the unwanted addresses
	print("Ping detected: %s" %source_ip) #DEBUG
	try:
		if source_ip in address_list.keys(): #if the IP is already in the list.
			address_list[source_ip][1] += 1

			if address_list[source_ip][1] >= MAX_PINGS_PER_TIME and time.time() - address_list[source_ip][0] <= TIME_PER_INTERVAL:
				lock.acquire()
				result = subprocess.check_output("sudo iptables -L", shell=True)
				if source_ip not in str(result) and source_ip not in banned_ips: #Checking if the ip is already in the ip tables.
					os.system("iptables -A INPUT -s " + source_ip + " -j DROP")
					print("DOS was detected! Blocking ip: %s" % source_ip)
					banned_ips.append(source_ip)
				lock.release()
			elif address_list[source_ip][1] < MAX_PINGS_PER_TIME and time.time() - address_list[source_ip][0] > TIME_PER_INTERVAL:
				address_list[source_ip] = [ time.time(), 1]
		else:
			address_list[source_ip] = [ time.time(), 1]
	except:
		print("Exception.")
		#address_list[source_ip] = [ time.time(), 1]


def print_event(cpu, data, size):
	global banned_ips
	data = ctypes.cast(data, ctypes.POINTER(MyStats)).contents
	src_ip = data.sIp
	src_ip = '.'.join([str(x) for x in src_ip.to_bytes(4, 'little')])
	#dst_ip = data.dIp
	#dst_ip = '.'.join([str(x) for x in dst_ip.to_bytes(4, 'little')])
	if not is_unwanted_address(src_ip) and src_ip not in banned_ips:
		check_and_block(src_ip)
		

def main():
	b = BPF(text = cCode)
	b.attach_kprobe(event="icmp_rcv", fn_name='icmp_trace') #Using the Kernel function "icmp_rcv" to track pings.

	b["events"].open_perf_buffer(print_event) 				#The function of the print is "print_event"
	print("Searching for PING spam...")
	while True:
	    try:
	        b.perf_buffer_poll()
	    except:
	        exit()


if __name__ == "__main__":
	main()